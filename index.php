<?php

require 'vendor/autoload.php';
require 'func.php';

use Goutte\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


// INIT
$gt = new Client();
$no = 0;
$id_kelas = xxxx;
$id_dosen = xxxx;
$pagination = 5;
$listUrlMhs = array();
$createData = array();
// 
$email = 'dosenxxxxx@unpam.ac.id';
$passw = 'xxxxxxxxxxxx';


// LOGIN KE LMS
$getDomain = $gt->request('GET', 'https://e-learning.unpam.ac.id/');
$readButton = $gt->click($getDomain->selectLink('Reguler C')->link());

// BACA LOGIN BUTTON
$formLogin = $readButton->selectButton('Log in')->form();

// LOGIN username dan password > Submit
$formVal = array('username' => $email, 'password' => $passw);
$fromSubmit = $gt->submit($formLogin, $formVal);


// BACA LIST USER/MAHASISWA
for ($page = 1; $page <= $pagination; $page++) { 
	$userLink = 'https://e-learningab.unpam.ac.id/user/index.php?id='.$id_kelas;
	if($page > 1) {
		$pg = $page - 1;
		$userLink = 'https://e-learningab.unpam.ac.id/user/index.php?id='.$id_kelas.'&page='.$pg;
	}

	$mahasiswa = $gt->request('GET', $userLink);
	// $tableMhs = $mahasiswa->filter('table tr th a')->extract(array('_text'));
	$tableMhs = $mahasiswa->filter('table')->filter('tr')->each(function ($tr, $i) {
	    return $tr->filter('th')->each(function ($td, $i) {
	        return $td->filter('a')->each(function ($a, $i) {
	            return $a->attr('href');
	        });
	    });
	});

	unset($tableMhs[0]);

	foreach ($tableMhs as $key => $val) {
		if(isset($val[0][0])) {
			if(!strpos($val[0][0], '?id='.$id_dosen)) {
				$uri = str_replace('https://e-learningab.unpam.ac.id/user/view.php?', 'https://e-learningab.unpam.ac.id/report/outline/user.php?', $val[0][0]);
				$listUrlMhs[$no++] = $uri.'&mode=outline';
			}
		}
	}
}


// NGE-LIST DATA
foreach ($listUrlMhs as $ky => $vl) {
	$idx = $ky;
	$detailMhs = $gt->request('GET', $vl);

	$idMhs = $detailMhs->filter('.ccnMdlHeading')->each(function($node){
	    return $node->text();
	});

	// NAMA
	$createData[$idx]['nama'] = 'unknown';
	$createData[$idx]['nim'] = 'unknown';
	if(isset($idMhs[0])) {
		$expId = explode(' ', $idMhs[0]);

		$namenya = '';
		foreach ($expId as $k => $v) {
			if(is_numeric(substr($v, 0,7))) {
				$explNim = explode('-', $v);
				$createData[$idx]['nim'] = $explNim[0];
			} else {
				$namenya .= $v.' ';
				$createData[$idx]['nama'] = $namenya;
			}
		}
	}

	// DATA
	$tableDtlMhs = $detailMhs->filter('table')->filter('tr')->each(function ($tr, $i) {
	    return $tr->filter('td')->each(function ($td, $i) {
	        return $td->text();;
	    });
	});

	$arr = 0;
	foreach ($tableDtlMhs as $key => $val) {
		if(isset($val[1])) {
			$subJudul = substr($val[1], 0,13);
			if($subJudul == 'FORUM DISKUSI') {
				$ke = substr($val[1], 13);

				$createData[$idx]['data'][$arr]['part'] = $val[1];
				$post = str_replace('posts', '', $val[3]);
				$post = str_replace(' ', '', $post);
				$createData[$idx]['data'][$arr]['create'] = intval($post) >= 2 ? $post : 0;

				$arr++;
			}
		}
	}
}

// EXCEL
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'No ');
$sheet->setCellValue('B1', 'Nama ');
$sheet->setCellValue('C1', 'NIM');
$sheet->setCellValue('D1', 'Forum Diskusi 1');
$sheet->setCellValue('E1', 'Forum Diskusi 2');
$sheet->setCellValue('F1', 'Forum Diskusi 3');
$sheet->setCellValue('G1', 'Forum Diskusi 4');
$sheet->setCellValue('H1', 'Forum Diskusi 5');
$sheet->setCellValue('I1', 'Forum Diskusi 6');
$sheet->setCellValue('J1', 'Forum Diskusi 7');
$sheet->setCellValue('K1', 'Forum Diskusi 8');
$sheet->setCellValue('L1', 'Forum Diskusi 9');
$sheet->setCellValue('M1', 'Forum Diskusi 10');
$sheet->setCellValue('N1', 'Forum Diskusi 11');
$sheet->setCellValue('O1', 'Forum Diskusi 12');
$sheet->setCellValue('P1', 'Forum Diskusi 13');
$sheet->setCellValue('Q1', 'Forum Diskusi 14');
$sheet->setCellValue('R1', 'Forum Diskusi 15');
$sheet->setCellValue('S1', 'Forum Diskusi 16');
$sheet->setCellValue('T1', 'Forum Diskusi 17');
$sheet->setCellValue('U1', 'Forum Diskusi 18');

$row = 2;
foreach ($createData as $kk => $vv) {
	$sheet->setCellValue('A'.$row, $row-1);
    $sheet->setCellValue('B'.$row, $vv['nama']);
    $sheet->setCellValueExplicit('C'.$row, $vv['nim'], PHPExcel_Cell_DataType::TYPE_STRING);
    $sheet->setCellValue('D'.$row, isset($vv['data'][0]) ? $vv['data'][0]['create'] : 0);
    $sheet->setCellValue('E'.$row, isset($vv['data'][1]) ? $vv['data'][1]['create'] : 0);
    $sheet->setCellValue('F'.$row, isset($vv['data'][2]) ? $vv['data'][2]['create'] : 0);
    $sheet->setCellValue('G'.$row, isset($vv['data'][3]) ? $vv['data'][3]['create'] : 0);
    $sheet->setCellValue('H'.$row, isset($vv['data'][4]) ? $vv['data'][4]['create'] : 0);
    $sheet->setCellValue('I'.$row, isset($vv['data'][5]) ? $vv['data'][5]['create'] : 0);
    $sheet->setCellValue('J'.$row, isset($vv['data'][6]) ? $vv['data'][6]['create'] : 0);
    $sheet->setCellValue('K'.$row, isset($vv['data'][7]) ? $vv['data'][7]['create'] : 0);
    $sheet->setCellValue('L'.$row, isset($vv['data'][8]) ? $vv['data'][8]['create'] : 0);
    $sheet->setCellValue('M'.$row, isset($vv['data'][9]) ? $vv['data'][9]['create'] : 0);
    $sheet->setCellValue('N'.$row, isset($vv['data'][10]) ? $vv['data'][10]['create'] : 0);
    $sheet->setCellValue('O'.$row, isset($vv['data'][11]) ? $vv['data'][11]['create'] : 0);
    $sheet->setCellValue('P'.$row, isset($vv['data'][12]) ? $vv['data'][12]['create'] : 0);
    $sheet->setCellValue('Q'.$row, isset($vv['data'][13]) ? $vv['data'][13]['create'] : 0);
    $sheet->setCellValue('R'.$row, isset($vv['data'][14]) ? $vv['data'][14]['create'] : 0);
    $sheet->setCellValue('S'.$row, isset($vv['data'][15]) ? $vv['data'][15]['create'] : 0);
    $sheet->setCellValue('T'.$row, isset($vv['data'][16]) ? $vv['data'][16]['create'] : 0);
    $sheet->setCellValue('U'.$row, isset($vv['data'][17]) ? $vv['data'][17]['create'] : 0);

    $row ++;
}

$writer = new Xlsx($spreadsheet);
$filename = 'Export-create-'.$id_kelas.'-'.$id_dosen.'-'.date('YmdHis');
ob_end_clean();
// header('Content-Type: application/vnd.ms-excel');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
header('Cache-Control: max-age=0');
$writer->save('php://output');
?>