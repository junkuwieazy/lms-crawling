<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.13.0',
            'version' => '4.13.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
            'dev_requirement' => false,
        ),
        'fabpot/goutte' => array(
            'pretty_version' => 'v4.0.1',
            'version' => '4.0.1.0',
            'type' => 'application',
            'install_path' => __DIR__ . '/../fabpot/goutte',
            'aliases' => array(),
            'reference' => '293e754f0be2f1e85f9b31262cb811de39874e03',
            'dev_requirement' => false,
        ),
        'maennchen/zipstream-php' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maennchen/zipstream-php',
            'aliases' => array(),
            'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
            'dev_requirement' => false,
        ),
        'markbaker/complex' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/complex',
            'aliases' => array(),
            'reference' => '6f724d7e04606fd8adaa4e3bb381c3e9db09c946',
            'dev_requirement' => false,
        ),
        'markbaker/matrix' => array(
            'pretty_version' => '2.1.3',
            'version' => '2.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/matrix',
            'aliases' => array(),
            'reference' => '174395a901b5ba0925f1d790fa91bab531074b61',
            'dev_requirement' => false,
        ),
        'myclabs/php-enum' => array(
            'pretty_version' => '1.8.3',
            'version' => '1.8.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/php-enum',
            'aliases' => array(),
            'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'phpoffice/phpspreadsheet' => array(
            'pretty_version' => '1.18.0',
            'version' => '1.18.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpspreadsheet',
            'aliases' => array(),
            'reference' => '418cd304e8e6b417ea79c3b29126a25dc4b1170c',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'symfony/browser-kit' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/browser-kit',
            'aliases' => array(),
            'reference' => 'c1e3f64fcc631c96e2c5843b666db66679ced11c',
            'dev_requirement' => false,
        ),
        'symfony/css-selector' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/css-selector',
            'aliases' => array(),
            'reference' => '7fb120adc7f600a59027775b224c13a33530dd90',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
            'dev_requirement' => false,
        ),
        'symfony/dom-crawler' => array(
            'pretty_version' => 'v5.3.7',
            'version' => '5.3.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/dom-crawler',
            'aliases' => array(),
            'reference' => 'c7eef3a60ccfdd8eafe07f81652e769ac9c7146c',
            'dev_requirement' => false,
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v5.3.8',
            'version' => '5.3.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'reference' => 'c6370fe2c0a445aedc08f592a6a3149da1fea4c7',
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'reference' => '7e82f6084d7cae521a75ef2cb5c9457bbda785f4',
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.4',
            ),
        ),
        'symfony/mime' => array(
            'pretty_version' => 'v5.3.8',
            'version' => '5.3.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'reference' => 'a756033d0a7e53db389618653ae991eba5a19a11',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
            'dev_requirement' => false,
        ),
    ),
);
