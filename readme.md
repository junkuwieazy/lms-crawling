1. Robot ini berfungsi pada LMS Universitas Pamulang pada tanun 2021 bulan Oktober

2. Jangan lupa mengubah data dosen terlebih dahulu.

3. Untuk mendapatkan $id_kelas, klik pada bagian Courses lalu lihat URL bar, https://e-learningab.unpam.ac.id/course/view.php?id=xxxx, xxxx adalah id_kelas.

4. Untuk mendapatkan $id_dosen, klik pada bagian pojok kanan atas logo user, klik profile lali lihat URL bar, https://e-learningab.unpam.ac.id/user/profile.php?id=xxx, xxx adalah id_dosen.

5. Parameter $pagination adalah banyaknya pagination list Mahasiswa, maximun biasanya 3, tapi robot akan mencari yang ada datanya saja, jika tidak ada data maka di skip.

6. Untuk memulai anda harus menginstall composer terlebih dahulu.

7. Setelah menginstall composer lalu ketikan pada command line, perlu diperhatikan path nya harus pada folder ini lalu ketikan composer install.

8. Isikan username/email dan password dosen.

9. Nyalakan Xampp/apache atau nginx jika menggunakan nginx.

10. Akses pada browser, jika berhasil maka akan mendownload file excel.